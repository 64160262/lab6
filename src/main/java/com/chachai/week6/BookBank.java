package com.chachai.week6;

public class BookBank {
    // Attributes
   private String name;
   private double balance;
    // public BookBank()
    public BookBank(String name , double balance ){  // constructor
        this.name = name;
        this.balance = balance;
    }
    // Methodes
    public boolean deposit(double money){
        if (money < 1) {
            return false;
        }
        balance = balance + money;
        return true;
    }
    public boolean withdraw(double money){
        if (money < 1) {
            return false;
        }
        if (money > balance) {
            return false;
        }
        balance = balance - money ;
        return true;
    }
    public void print(){
        System.out.println(name + " " + balance);
    }

    public String getName(){
        return name;
    }
    public double getBalance(){
        return balance;
    }
}
