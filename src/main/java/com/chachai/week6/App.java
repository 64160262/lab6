package com.chachai.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       BookBank praewwarin =  new BookBank("Praewwarin" , 50.0 ); // Constructor
       praewwarin.print();
       BookBank prayud =  new BookBank("Prayud" , 100000.0 );
       prayud.print();
       prayud.withdraw(40000.0);
       prayud.print();

       praewwarin.deposit(40000.0);
       praewwarin.print();
       
       BookBank prawit = new BookBank("Prawit" , 1000000.0  );
       prawit.print();
       prawit.deposit(2);
       prawit.print();

    }
}
